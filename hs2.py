import pandas as pd
import getpass
import shapefile
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.dates as dates
import datetime as dt
import pandas as pd
import geopandas
from shapely.geometry import Point, Polygon
from geopandas import GeoDataFrame as gdf
import geoplot as gplt
import geoplot.crs as gcrs
from pyproj import Proj, transform
import mapclassify as mc
from statsmodels.tsa.stattools import adfuller
from statsmodels.tsa.seasonal import seasonal_decompose
from statsmodels.tsa.arima_model import ARIMA
import statsmodels.api as sm
from pandas.plotting import register_matplotlib_converters

uname=getpass.getuser()


def reject_outliers(data, m=2):
    return data[abs(data - np.mean(data)) < m * np.std(data)]

def moving_average(a, n=500) :
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n

def get_stationarity(timeseries):

    # rolling statistics
    rolling_mean = timeseries.rolling(window=12).mean()
    rolling_std = timeseries.rolling(window=12).std()

    # rolling statistics plot
    original = plt.plot(timeseries, color='blue', label='Original')
    mean = plt.plot(rolling_mean, color='red', label='Rolling Mean')
    std = plt.plot(rolling_std, color='black', label='Rolling Std')
    plt.legend(loc='best')
    plt.title('Rolling Mean & Standard Deviation')
    plt.show(block=False)

    # Dickey–Fuller test:
    result = adfuller(timeseries['AverageSalePrice'])
    print('ADF Statistic: {}'.format(result[0]))
    print('p-value: {}'.format(result[1]))
    print('Critical Values:')
    for key, value in result[4].items():
        print('\t{}: {}'.format(key, value))


# Read the data
dfLRP = pd.read_csv('../Land Registry Price Paid Data.csv', sep=',',low_memory=False)
dfPTY = pd.read_csv('../VOA Property Type Data.csv', sep=',',low_memory=False)
dfPAY = pd.read_csv('../VOA Property Age Data.csv', sep=',',low_memory=False)
sf = shapefile.Reader("../HS2.shp")

# Output the columns with their respective type

dfLRP.dtypes



#Route of HS2 in Birmingham
plt.figure()
plt.title("HS2 planned route",fontsize=18)
for shape in sf.shapeRecords():
    x = [i[0] for i in shape.shape.points[:]]
    y = [i[1] for i in shape.shape.points[:]]
    plt.plot(x,y)
plt.xlabel("Eastings",fontsize=14)
plt.ylabel("Northings",fontsize=14)
plt.tight_layout()
plt.show()


shapes = sf.shapes()
# sf.shape(0).bbox
# shapes[0].bbox
hs2utmpoints = shapes[0].points



dfPD = dfLRP[['SalePrice','SaleDate']]


# Histogram of house prices in Birmingham
pricehist = dfPD.SalePrice.values

#Remove house prices more or less than 2std
pricehist = reject_outliers(pricehist)
plt.hist(pricehist)
plt.title("Histogram of house prices",fontsize=18)
plt.xlabel("House price (pounds)",fontsize=14)
plt.ylabel("Frequency",fontsize=14)
plt.xlim(xmin=pricehist.min(), xmax = pricehist.max())
plt.tight_layout()
plt.show()


# keep only the ones that are within +2 to -2 standard deviations in the column 'SalePrice'.
# dfPD = dfPD[np.abs(dfPD.SalePrice-dfPD.SalePrice.mean()) <= (2*dfPD.SalePrice.std())]
# dfPD = dfPD[~(np.abs(dfPD.SalePrice-dfPD.SalePrice.mean()) > (2*dfPD.SalePrice.std()))]


dfPD = dfPD.sort_values(by=['SaleDate'])



dates = dfPD['SaleDate'].values
x = [dt.datetime.strptime(d,'%Y-%m-%d').date() for d in dates]



dfPD['SaleDate'] = pd.to_datetime(dfPD.SaleDate)
# dfPD = dfPD.groupby(dfPD.SaleDate.dt.year)['SalePrice'].transform('mean')
dfPD = dfPD.groupby([(dfPD.SaleDate.dt.year),(dfPD.SaleDate.dt.month),(dfPD.SaleDate.dt.week)])['SalePrice'].transform('mean')
y = dfPD.values

# import pdb;pdb.set_trace()
y_moving = moving_average(y)
x_moving = x[250:]
x_moving = x_moving[-118947:]


plt.subplot(2,1,1)
plt.plot(x,y)
plt.title("House prices trend (weekly)",fontsize=18)
plt.xlabel("Date (Year)",fontsize=14)
plt.ylabel("Mean house price (Pounds)",fontsize=14)
plt.tight_layout()


plt.subplot(2,1,2)
plt.plot(x_moving,y_moving)
plt.title("Moving average (house prices)",fontsize=18)
plt.xlabel("Date (Year)",fontsize=14)
plt.ylabel("Mean house price (Pounds)",fontsize=14)
plt.tight_layout()
plt.show()


#It would be nice if we compared the house prices trend with another city where HS2 does not pass or the average house of the UK.

# A company named HS2 was set up in January 2009 by the Department for
# Transport (DfT) for the development of the HS2 project.
# The company submitted the proposals to the government in the
# first quarter of 2010 and the preferred route option was announced in October 2010.

# Split in circles the areas where the HS2 passes.

# LSOA E01008914 central Birmingham near Curzon Street station




dfGEO = geopandas.read_file("../HS2.shp")
# dfBirm = geopandas.read_file("../E08000025/Woodland.shp")
# dfBirm = geopandas.read_file("../E08000025/Road.shp")
dfBirmN = geopandas.read_file("../E08000025/NamedPlace.shp")
dfBirm = geopandas.read_file("../E08000025_2/E08000025_Birmingham.shp")
# dfBirmN['geometry'].plot(color='yellow',edgecolor='k',linewidth = 0.5,ax=ax)

dfBirmLatLon = dfBirm.to_crs(epsg=4326)
dfGEOLatLon = dfGEO.to_crs(epsg=4326)



# inProj = Proj(init='epsg:27700')
# outProj = Proj(init='epsg:4326')

# geometry = dfBirm.geometry

# #Iterate through your column of points and convert:

# new_geo = []
# empty_list = []
# for pt in geometry:

#     #need to pull out data form Shapely object
#     # coords_obj = list(pt.coords)
#     # coords_obj = pt.exterior.coords
#     coords_obj = np.array(pt.exterior.coords).tolist()
#     # transform points us pyroj
#     x,y = transform(inProj,outProj, coords_obj[0][0], coords_obj[0][1])
#     # put in your empty list as the desired Shapely object
#     new_geo.append(Point(x,y))
#     import pdb;pdb.set_trace()

# empty_list =pd.Series(empty_list)
#  # replace your geopandas column
# geometry = new_geo
# import pdb;pdb.set_trace()
# geometry = Polygon(new_geo)







# import pdb;pdb.set_trace()


# f, ax = plt.subplots(1)

# ax = dfBirm.plot(edgecolor='grey')
# dfGEO['geometry'].plot(color='black',edgecolor='k',linewidth = 2,ax=ax)
# dftemp = dfBirmN.head(20)
# dftemp.plot(ax=ax)
# dftemp.apply(lambda x: ax.annotate(s=x.htmlname, xy=x.geometry.centroid.coords[0], ha='center'),axis=1)

# keep only the ones that are within +2 to -2 standard deviations in the column 'SalePrice'.
dfLRP2 = dfLRP[np.abs(dfLRP.SalePrice-dfLRP.SalePrice.mean()) <= (2*dfLRP.SalePrice.std())]
dfLRP2 = dfLRP2[~(np.abs(dfLRP2.SalePrice-dfLRP2.SalePrice.mean()) > (2*dfLRP2.SalePrice.std()))]
dates = dfLRP2['SaleDate'].values
dfLRP2.SaleDate = [dt.datetime.strptime(d,'%Y-%m-%d').date() for d in dates]


# Select only dates in specific year
dfLRP2009 = dfLRP2.loc[dfLRP2['SaleDate'].map(lambda x: x.year) == 2009]
dfLRP2011 = dfLRP2.loc[dfLRP2['SaleDate'].map(lambda x: x.year) == 2011]
dfLRP2013 = dfLRP2.loc[dfLRP2['SaleDate'].map(lambda x: x.year) == 2013]
dfLRP2015 = dfLRP2.loc[dfLRP2['SaleDate'].map(lambda x: x.year) == 2015]
dfLRP2017 = dfLRP2.loc[dfLRP2['SaleDate'].map(lambda x: x.year) == 2017]
dfLRP2019 = dfLRP2.loc[dfLRP2['SaleDate'].map(lambda x: x.year) == 2019]

# df_lst = [dfLRP2009,dfLRP2011,dfLRP2013,dfLRP2015,dfLRP2017,dfLRP2019]






dfheat = dfLRP2[['SalePrice','Longitude','Latitude']]
dfheat['geometry']= [Point(xy) for xy in zip(dfheat.Longitude, dfheat.Latitude)]
geometry = [Point(xy) for xy in zip(dfheat.Longitude, dfheat.Latitude)]
crs = {'init': 'epsg:4326'}
gdfheat = gdf(dfheat, crs=crs, geometry=geometry)
gdfheat = gdfheat.drop(['Longitude', 'Latitude'], axis=1)
gdfheatr = gdfheat.reset_index(drop=True)

ax = gplt.kdeplot(gdfheat,clip=dfBirmLatLon.geometry,shade=True, cmap='Reds',projection=gcrs.AlbersEqualArea())
plt.title("Property sales in Birmingham",fontsize=18)
gplt.polyplot(dfGEOLatLon, ax=ax, zorder=2,edgecolor='blue')
gplt.polyplot(dfBirmLatLon, ax=ax, zorder=1)
plt.savefig("kdeBirmingham.png", bbox_inches='tight', pad_inches=0.1,dpi=300)
plt.show()


# Plot 2009 sale price mapped with location
dfheat = dfLRP2009[['SalePrice','Longitude','Latitude']]
dfheat['geometry']= [Point(xy) for xy in zip(dfheat.Longitude, dfheat.Latitude)]
geometry = [Point(xy) for xy in zip(dfheat.Longitude, dfheat.Latitude)]
crs = {'init': 'epsg:4326'}
gdfheat = gdf(dfheat, crs=crs, geometry=geometry)
gdfheat = gdfheat.drop(['Longitude', 'Latitude'], axis=1)
gdfheatr = gdfheat.reset_index(drop=True)

scheme = mc.NaturalBreaks(gdfheatr['SalePrice'], k=10)
ax = gplt.quadtree(
    gdfheatr, nmax=1,
    projection=gcrs.AlbersEqualArea(), clip=dfBirmLatLon,
    hue='SalePrice', cmap='Reds',
    edgecolor='white', legend=True,figsize=(11,11)
)
gplt.polyplot(dfGEOLatLon, ax=ax, zorder=2,edgecolor='blue')
gplt.polyplot(dfBirmLatLon, edgecolor='black', zorder=1, ax=ax)
plt.title("Property sale price (2009)",fontsize=18)
plt.savefig("quadBirmingham2009.png", bbox_inches='tight', pad_inches=0.1,dpi=300)
plt.show()

# Plot 2019 sale price mapped with location
dfheat = dfLRP2019[['SalePrice','Longitude','Latitude']]
dfheat['geometry']= [Point(xy) for xy in zip(dfheat.Longitude, dfheat.Latitude)]
geometry = [Point(xy) for xy in zip(dfheat.Longitude, dfheat.Latitude)]
crs = {'init': 'epsg:4326'}
gdfheat = gdf(dfheat, crs=crs, geometry=geometry)
gdfheat = gdfheat.drop(['Longitude', 'Latitude'], axis=1)
gdfheatr = gdfheat.reset_index(drop=True)

scheme = mc.NaturalBreaks(gdfheatr['SalePrice'], k=10)
ax = gplt.quadtree(
    gdfheatr, nmax=1,
    projection=gcrs.AlbersEqualArea(), clip=dfBirmLatLon,
    hue='SalePrice', cmap='Reds',
    edgecolor='white', legend=True,figsize=(11,11)
)
gplt.polyplot(dfGEOLatLon, ax=ax, zorder=2,edgecolor='blue')
gplt.polyplot(dfBirmLatLon, edgecolor='black', zorder=1, ax=ax)
plt.title("Property sale price (2019)",fontsize=18)
plt.savefig("quadBirmingham2019.png", bbox_inches='tight', pad_inches=0.1,dpi=300)
plt.show()









# Find percentage increase of average selling price from year to year
# Percentage increase = (New number - Original number) / Original number * 100
avprice2009 = np.mean(dfLRP2009.SalePrice.values)
avprice2011 = np.mean(dfLRP2011.SalePrice.values)
avprice2013 = np.mean(dfLRP2013.SalePrice.values)
avprice2015 = np.mean(dfLRP2015.SalePrice.values)
avprice2017 = np.mean(dfLRP2017.SalePrice.values)
avprice2019 = np.mean(dfLRP2019.SalePrice.values)

perinc2011 = (avprice2011-avprice2009)/avprice2009*100
perinc2013 = (avprice2013-avprice2011)/avprice2011*100
perinc2015 = (avprice2015-avprice2013)/avprice2013*100
perinc2017 = (avprice2017-avprice2015)/avprice2015*100
perinc2019 = (avprice2019-avprice2017)/avprice2017*100

print(str(round(perinc2011,2))+"% increase from 2009 to 2011")
print(str(round(perinc2013,2))+"% increase from 2011 to 2013")
print(str(round(perinc2015,2))+"% increase from 2013 to 2015")
print(str(round(perinc2017,2))+"% increase from 2015 to 2017")
print(str(round(perinc2019,2))+"% increase from 2017 to 2019")


import pdb;pdb.set_trace()


dfARIMA = dfLRP2[['SaleDate','SalePrice']]
dfARIMA = dfARIMA.sort_values(by=['SaleDate'])
dfARIMA['SaleDate'] = pd.to_datetime(dfARIMA.SaleDate)
dfARIMA['AverageSalePrice'] = dfARIMA.groupby([(dfARIMA.SaleDate.dt.year),(dfARIMA.SaleDate.dt.month),(dfARIMA.SaleDate.dt.week)])['SalePrice'].transform('mean')
dfARIMA = dfARIMA.drop_duplicates('AverageSalePrice',keep='first')
dfARIMA = dfARIMA.drop(['SalePrice'], axis=1)
dfARIMA = dfARIMA.drop(['SaleDate'], axis=1)
# dfARIMA = dfARIMA.set_index('SaleDate')
dfARIMA = dfARIMA.reset_index(drop=True)
dfARIMA.head()


df = dfARIMA
df['value'] = dfARIMA['AverageSalePrice']
df = df.drop(['AverageSalePrice'], axis=1)
df['value'] = df['value'] * 0.001


# Check if our timeseries is stationary, if P Value > 0.05 (Non-stationary) we go ahead with finding the order of differencing.
from statsmodels.tsa.stattools import adfuller
from numpy import log
result = adfuller(df.value.dropna())
print('ADF Statistic: %f' % result[0])
print('p-value: %f' % result[1])


# Since P-value is greater than the significance level, let’s difference the series and see how the autocorrelation plot looks like.
import numpy as np, pandas as pd
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
import matplotlib.pyplot as plt

# plt.rcParams.update({'figure.figsize':(9,7), 'figure.dpi':120})

# Original Series
fig, axes = plt.subplots(3, 2, sharex=True)
axes[0, 0].plot(df.value); axes[0, 0].set_title('Original Series')
plot_acf(df.value, ax=axes[0, 1])

# 1st Differencing
axes[1, 0].plot(df.value.diff()); axes[1, 0].set_title('1st Order Differencing')
plot_acf(df.value.diff().dropna(), ax=axes[1, 1])

# 2nd Differencing
axes[2, 0].plot(df.value.diff().diff()); axes[2, 0].set_title('2nd Order Differencing')
plot_acf(df.value.diff().diff().dropna(), ax=axes[2, 1])

plt.show()


# from pmdarima.arima.utils import ndiffs
# y = df.value
# ## Adf Test
# ndiffs(y, test='adf')  # 0

# # KPSS test
# ndiffs(y, test='kpss')  # 1

# # PP test:
# ndiffs(y, test='pp')  # 0


# find the order of the AR term (p)
# PACF plot of 1st differenced series
# plt.rcParams.update({'figure.figsize':(9,3), 'figure.dpi':120})

fig, axes = plt.subplots(1, 2, sharex=True)
axes[0].plot(df.value.diff()); axes[0].set_title('1st Differencing')
# axes[1].set(ylim=(0,5))
plot_pacf(df.value.diff().dropna(), ax=axes[1])

plt.show()

# You can observe that the PACF lag 1 is quite significant since is well above the significance line. Lag 2 turns out to be significant as well, slightly managing to cross the significance limit (blue region). But I am going to be conservative and tentatively fix the p as 1.

 # find the order of the MA term (q)
import pandas as pd
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
import matplotlib.pyplot as plt


fig, axes = plt.subplots(1, 2, sharex=True)
axes[0].plot(df.value.diff()); axes[0].set_title('1st Differencing')
# axes[1].set(ylim=(0,1.2))
plot_acf(df.value.diff().dropna(), ax=axes[1])

plt.show()

# Couple of lags are well above the significance line. So, let’s tentatively fix q as 2. When in doubt, go with the simpler model that sufficiently explains the Y.





from statsmodels.tsa.arima_model import ARIMA
model = ARIMA(df.value, order=(1,1,1))
model_fit = model.fit(disp=0)
print(model_fit.summary())


# Plot residual errors
residuals = pd.DataFrame(model_fit.resid)
fig, ax = plt.subplots(1,2)
residuals.plot(title="Residuals", ax=ax[0])
residuals.plot(kind='kde', title='Density', ax=ax[1])
plt.show()



# Actual vs Fitted
model_fit.plot_predict(dynamic=False)
plt.show()



# from sklearn.model_selection import train_test_split
# train, test = train_test_split(df, test_size=0.2,shuffle='False')

from statsmodels.tsa.stattools import acf


train = df.value[:550]
test = df.value[550:]

# Build Model
# model = ARIMA(train, order=(3,2,1))
# The (p,d,q) order of the model for the number of AR parameters, differences, and MA parameters to use.
model = ARIMA(train, order=(1, 1, 1))
fitted = model.fit(disp=-1)

# Forecast
fc, se, conf = fitted.forecast(98, alpha=0.05)  # 95% conf

# Make as pandas series
fc_series = pd.Series(fc, index=test.index)
lower_series = pd.Series(conf[:, 0], index=test.index)
upper_series = pd.Series(conf[:, 1], index=test.index)

# Plot
plt.figure(figsize=(12,5), dpi=100)
plt.plot(train, label='training')
plt.plot(test, label='actual')
plt.plot(fc_series, label='forecast')
plt.fill_between(lower_series.index, lower_series, upper_series,color='k', alpha=.15)
plt.title('Forecast vs Actuals')
plt.legend(loc='upper left', fontsize=8)
plt.show()


# From the chart, the ARIMA(1,1,1) model seems to give a directionally correct forecast. And the actual observed values lie within the 95% confidence band. That seems fine.


#Build another ARIMA model with different p,q,d
# Build Model
model = ARIMA(train, order=(2, 1, 1))
fitted = model.fit(disp=-1)
print(fitted.summary())

# Forecast
fc, se, conf = fitted.forecast(98, alpha=0.05)  # 95% conf

# Make as pandas series
fc_series = pd.Series(fc, index=test.index)
lower_series = pd.Series(conf[:, 0], index=test.index)
upper_series = pd.Series(conf[:, 1], index=test.index)

# Plot
plt.figure(figsize=(12,5), dpi=100)
plt.plot(train, label='training')
plt.plot(test, label='actual')
plt.plot(fc_series, label='forecast')
plt.fill_between(lower_series.index, lower_series, upper_series,color='k', alpha=.15)
plt.title('Forecast vs Actuals')
plt.legend(loc='upper left', fontsize=8)
plt.show()


#Auto ARIMA
import pmdarima as pm
model = pm.auto_arima(df.value, start_p=1, start_q=1,
                      test='adf',       # use adftest to find optimal 'd'
                      max_p=3, max_q=3, # maximum p and q
                      m=1,              # frequency of series
                      d=None,           # let model determine 'd'
                      seasonal=False,   # No Seasonality
                      start_P=0,
                      D=0,
                      trace=True,
                      error_action='ignore',
                      suppress_warnings=True,
                      stepwise=True)

print(model.summary())




########################
########################
########################

# Arima model
plt.xlabel('Date')
plt.ylabel('Average Sale Price (Weekly)')
plt.plot(dfARIMA)
rolling_mean = dfARIMA.rolling(window = 12).mean()
rolling_std = dfARIMA.rolling(window = 12).std()
plt.plot(dfARIMA, color = 'blue', label = 'Original')
plt.plot(rolling_mean, color = 'red', label = 'Rolling Mean')
plt.plot(rolling_std, color = 'black', label = 'Rolling Std')
plt.legend(loc = 'best')
plt.title('Rolling Mean & Rolling Standard Deviation',fontsize=18)
plt.tight_layout()
plt.savefig("arimaMEAN.png", bbox_inches='tight', pad_inches=0.1,dpi=300)
plt.show()

result = adfuller(dfARIMA['AverageSalePrice'])
print('ADF Statistic: {}'.format(result[0]))
print('p-value: {}'.format(result[1]))
print('Critical Values:')
for key, value in result[4].items():
    print('\t{}: {}'.format(key, value))

df_log = np.log(dfARIMA)
plt.plot(df_log)



# Subtract the rolling mean
rolling_mean = df_log.rolling(window=12).mean()
df_log_minus_mean = df_log - rolling_mean
df_log_minus_mean.dropna(inplace=True)
get_stationarity(df_log_minus_mean)

# Exponential decay is another way of transforming a time series such that it is stationary
rolling_mean_exp_decay = df_log.ewm(halflife=12, min_periods=0, adjust=True).mean()
df_log_exp_decay = df_log - rolling_mean_exp_decay
df_log_exp_decay.dropna(inplace=True)
get_stationarity(df_log_exp_decay)

# When applying time shifting, we subtract every the point by the one that preceded it.
df_log_shift = df_log - df_log.shift()
df_log_shift.dropna(inplace=True)
get_stationarity(df_log_shift)



# AutoRegressive Model (AR)

decomposition = seasonal_decompose(df_log,freq=30)
model = ARIMA(df_log, order=(2,1,2))
results = model.fit(disp=-1)
plt.plot(df_log_shift)
plt.plot(results.fittedvalues, color='red')

predictions_ARIMA_diff = pd.Series(results.fittedvalues, copy=True)
predictions_ARIMA_diff_cumsum = predictions_ARIMA_diff.cumsum()
predictions_ARIMA_log = pd.Series(df_log['AverageSalePrice'].iloc[0], index=df_log.index)
predictions_ARIMA_log = predictions_ARIMA_log.add(predictions_ARIMA_diff_cumsum, fill_value=0)
# Adding mean

predictions_ARIMA = np.exp(predictions_ARIMA_log)
plt.plot(dfARIMA)
plt.plot(predictions_ARIMA)

results.plot_predict(1,300)
plt.title('Arima prediction',fontsize=18)
plt.tight_layout()
plt.savefig("arimaPredict.png", bbox_inches='tight', pad_inches=0.1,dpi=300)


# gplt.choropleth(
#     gdfheatr.head(2000),
#     hue='SalePrice',
#     cmap='Purples', linewidth=0.5,
#     edgecolor='white',
#     legend=True,
#     projection=gcrs.AlbersEqualArea()
# )
# gplt.polyplot(dfGEOLatLon, ax=ax, zorder=2,edgecolor='blue')
# gplt.polyplot(dfBirmLatLon, edgecolor='black', zorder=1, ax=ax)
# plt.savefig("choroBirmingham.png", bbox_inches='tight', pad_inches=0.1,dpi=300)
# plt.show()


# 978


# gdfheatr['id'] = gdfheatr.index
# cols = gdfheatr.columns.tolist()
# cols.insert(0, cols.pop(cols.index('id')))
# gdfheatr = gdfheatr.reindex(columns= cols)
# gdfheatr.iloc[:978,:].values
# gdfheatr['SalePrice'] = gdfheatr['SalePrice'].astype('int32')
# gd = gdfheatr.apply(lambda col: col.drop_duplicates().reset_index(drop=True))

# scheme = mc.NaturalBreaks(gdfheatr['SalePrice'], k=10)
# ax = gplt.voronoi(
#     gdfheatr.head(978), projection=gcrs.AlbersEqualArea(),
#     clip=dfBirmLatLon.geometry,
#     hue='SalePrice', scheme=scheme, cmap='Reds',
#     legend=True,
#     edgecolor='white', legend_kwargs={'loc': 'upper left'}
# )
# # *** ValueError: Length of values does not match length of index
# gplt.polyplot(dfBirmLatLon, edgecolor='black', zorder=1, ax=ax)
# gplt.polyplot(dfGEOLatLon, ax=ax, zorder=2,edgecolor='blue')



# collisions = geopandas.read_file(gplt.datasets.get_path('nyc_injurious_collisions'))





# ax = gplt.quadtree(
#     gdfheat, nmax=1,
#     projection=gcrs.AlbersEqualArea(), clip=dfBirmLatLon,
#     facecolor='lightgray', edgecolor='white', zorder=1
# )
# gplt.pointplot(collisions, s=1, ax=ax)
# .py:290: UserWarning: GeoSeries.isna() previously returned True for both missing (None) and empty geometries. Now, it only returns True for missing values. Since the calling GeoSeries contains empty geometries, the result has changed compared to previous versions of GeoPandas.
# Given a GeoSeries 's', you can use 's.is_empty | s.isna()' to get back the old behaviour.










