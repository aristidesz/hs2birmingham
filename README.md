# HS2Birmingham
## What is happening in the Birmingham property market?

HS2 Birmingham Real Estate C.ipynb notebook includes the work done in this project.

Data used:

- Land Registry Price Paid data for Birmingham Local Authority for last ten years
- A shapefile showing the planned route of HS2 (The new high speed rail to London)


The notebook includes plots for the:

- route of HS2 in Birmingham
- histogram of house prices.
- house prices trend and moving average
- property transactions for various years
- property selling price mapped in location for years 2009 and 2019
- ARIMA model predictions.





